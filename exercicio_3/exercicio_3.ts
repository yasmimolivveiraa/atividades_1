//Faça um programa que receba três notas, calcule e mostre a média ponderada entre elas.

namespace exercicio_3
{
    let nota1 ,peso1, nota2, peso2: number; let nota3, peso3: number;
    
    nota1 = 10;
    nota2 = 6; 
    nota3 = 7;
    peso1 = 3; 
    peso2 = 1;
    peso3 = 6;

    let mediaP: number;
    mediaP = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3); 

    console.log(`A média ponderada é: ${mediaP}`)
}

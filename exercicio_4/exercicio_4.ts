//Faça um programa que receba o salário de um funcionário, calcule e mostre o novo salário, sabendo-se que este sofreu um aumento de 25%.

namespace exercico_4 
{
    let salario:number

    salario = 1500;

    let novoSalario: number;

    novoSalario = salario + salario * 25/100;
    
    console.log(`O novo salario é de: ${novoSalario}`)
}